local cmd = vim.cmd
cmd [[
let g:tmuxline_powerline_separators = 0
let g:tmuxline_preset = {
      \'a'    : '#S',
      \'b'    : '#W',
      \'c'    : '',
      \'win'  : '#I #W',
      \'cwin' : '#I #W',
      \'x'    : '#H',
      \'y'    : '%a',
      \'z'    : '%R'}
]]
