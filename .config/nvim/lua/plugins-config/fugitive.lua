vim.keymap.set("n", "<leader>gg", ":G<CR>", { silent = true, noremap = true })
vim.keymap.set("n", "<leader>gp", ":G push<CR>", { silent = true, noremap = true })
vim.keymap.set("n", "<leader>gf", ":G fetch<CR>", { silent = true, noremap = true })
vim.keymap.set("n", "<leader>gb", ":G co -b<CR>", { silent = true, noremap = true })
