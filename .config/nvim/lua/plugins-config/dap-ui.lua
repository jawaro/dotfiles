require("dapui").setup()

local dap = require("dap")
local dapui = require("dapui")

dap.listeners.after.event_initialized["dapui_config"] = function()
	dapui.open()
end
dap.listeners.after.event_terminated["dapui_config"] = function()
	dapui.close()
end
dap.listeners.after.event_exited["dapui_config"] = function()
	dapui.close()
end

vim.keymap.set("n", "<leader>dt", ":DapToggleBreakpoint<CR>")
vim.keymap.set("n", "<leader>dx", ":DapTerminate<CR>")
vim.keymap.set("n", "<leader>do", ":DapStepOver<CR>")

-- Elixir
dap.adapters.mix_task = {
	type = 'executable',
	-- command = vim.fn.stdpath("data") .. '/mason/bin/elixir-ls-debugger',
	command = vim.fn.stdpath("cache") ..
		'/elixir-tools.nvim/installs/elixir-lsp/elixir-ls/tags_v0.19.0/1.16.1-26/debug_adapter.sh',
	args = {}
}
dap.configurations.elixir = {
	-- {
	-- 	type = "mix_task",
	-- 	name = "mix test",
	-- 	request = "launch",
	-- 	task = 'test',
	-- 	taskArgs = { "--trace" },
	-- 	startApps = true, -- for Phoenix projects
	-- 	projectDir = "${workspaceFolder}",
	-- 	requireFiles = {
	-- 		"test/**/test_helper.exs",
	-- 		"test/**/*_test.exs"
	-- 	}
	-- },
	{
		type = "mix_task",
		name = "phx.server",
		request = "launch",
		task = 'phx.server',
		startApps = false, -- for Phoenix projects
		projectDir = "${workspaceFolder}",
	},
}
