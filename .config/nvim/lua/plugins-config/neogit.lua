local neogit = require("neogit")

vim.keymap.set("n", "<leader>gg", neogit.open, { silent = true, noremap = true })
vim.keymap.set("n", "<leader>gp", ":Neogit push<CR>", { silent = true, noremap = true })
vim.keymap.set("n", "<leader>gf", ":Neogit fetch<CR>", { silent = true, noremap = true })
vim.keymap.set("n", "<leader>gb", ":Neogit co -b<CR>", { silent = true, noremap = true })
