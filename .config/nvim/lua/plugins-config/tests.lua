local g = vim.g
local nmap = require("utils").nmap

g["test#strategy"] = "dispatch"
g["test#filename_modifier"] = ":p"
g["test#preserve_screen"] = 1
g["test#neovim#term_position"] = "belowright"
g["test#neovim#preserve_screen"] = 1
g["test#javascript#jest#options"] = "--reporters jest-vim-reporter"

g["dispatch_compilers#elixir"] = "exunit"
g["dispatch_compilers#mix"] = "exunit"

nmap("<leader>tn", ":TestNearest<CR>")
nmap("<leader>tf", ":TestFile<CR>")
nmap("<leader>ta", ":TestSuite<CR>")
nmap("<leader>tl", ":TestLast<CR>")
nmap("<leader>tv", ":TestVisit<CR>")
