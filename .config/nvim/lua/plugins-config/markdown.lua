local nmap = require("utils").nmap
local imap = require("utils").imap

vim.g["mkdp_browser"] = "/usr/bin/firefox"
-- don't close preview when switching to another buffer
vim.g["mkdp_auto_close"] = 0
vim.g["mkdp_markdown_css"] = '/home/ah/.config/markdown/markdown.css'

nmap("<F8>", "<Plug>MarkdownPreviewToggle")
