local icons = require("icons")

local lspconfig = require("lspconfig")
local handlers = require("lsp.handlers")

local status, mason = pcall(require, "mason")
if not status then
	return
end

local status2, mason_lspconfig = pcall(require, "mason-lspconfig")
if not status2 then
	return
end

mason.setup({})

-- Enable the following language servers
local servers = {
	"clangd",
	"cssls",
	"dockerls",
	-- "elixirls",
	"gopls",
	"jsonls",
	-- "marksman",
	"ocamllsp",
	"rust_analyzer",
	"tailwindcss",
	"ts_ls",
}

mason_lspconfig.setup({
	ensure_installed = servers,
})

for _, lsp in ipairs(servers) do
	if lsp ~= "ts_ls" and lsp ~= "tailwindcss" and lsp ~= "rust_analyzer" and lsp ~= "elixirls" then
		lspconfig[lsp].setup({
			on_attach = handlers.on_attach,
			capabilities = handlers.capabilities,
		})
	end
end

lspconfig.cssls.setup({
	settings = {
		css = {
			lint = {
				unknownAtRules = "ignore",
			},
		},
	},
})

-- lspconfig.elixirls.setup({
-- 	on_attach = handlers.on_attach,
-- 	capabilities = handlers.capabilities,
-- 	flags = {
-- 		debounce_text_changes = 150,
-- 	},
-- 	filetypes = { "elixir", "eelixir", "heex" },
-- 	elixirLS = {
-- 		dialyzerEnabled = true,
-- 		fetchDeps = false,
-- 		enableTestLenses = true,
-- 	},
-- })
-- })

lspconfig.rust_analyzer.setup({
	on_attach = handlers.on_attach,
	capabilities = handlers.capabilities,
	flags = {
		debounce_text_changes = 150,
	},
})

lspconfig.tailwindcss.setup({
	on_attach = handlers.on_attach,
	capabilities = handlers.capabilities,
	experimental = {
		classRegex = {
			[[class="([^"]*)]],
			'class=\\s+"([^"]*)',
		},
	},
	init_options = {
		userLanguages = {
			html = "html",
			heex = "phoenix-heex",
			elixir = "html-eex",
			eelixir = "html-eex",
			eruby = "erb",
		},
	},
	settings = {
		tailwindCSS = {
			classAttributes = { "class", "className", "classList", "ngClass" },
			lint = {
				cssConflict = "warning",
				invalidApply = "error",
				invalidConfigPath = "error",
				invalidScreen = "error",
				invalidTailwindDirective = "error",
				invalidVariant = "error",
				recommendedVariantOrder = "warning",
			},
			validate = true,
		},
	},
})

lspconfig.ts_ls.setup({
	on_attach = handlers.on_attach,
	filetypes = { "javascript", "typescript", "typescriptreact", "typescript.tsx" },
	cmd = { "typescript-language-server", "--stdio" },
})

-- Make runtime files discoverable to the server
local runtime_path = vim.split(package.path, ";")
table.insert(runtime_path, "lua/?.lua")
table.insert(runtime_path, "lua/?/init.lua")

lspconfig.lua_ls.setup({
	on_attach = handlers.on_attach,
	capabilities = handlers.capabilities,
	settings = {
		Lua = {
			runtime = {
				-- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
				version = "LuaJIT",
				-- Setup your lua path
				path = runtime_path,
			},
			diagnostics = {
				-- Get the language server to recognize the `vim` global
				globals = { "vim" },
			},
			workspace = {
				-- Make the server aware of Neovim runtime files
				library = vim.api.nvim_get_runtime_file("", true),
			},
			-- Do not send telemetry data containing a randomized but unique identifier
			telemetry = {
				enable = false,
			},
		},
	},
})

-- luasnip setup
local luasnip = require("luasnip")

-- nvim-cmp setup
local cmp = require("cmp")
-- local lspkind = require('lspkind')
cmp.setup({
	-- formatting = {
	-- 	format = lspkind.cmp_format({
	--
	-- 		mode = 'symbol',
	-- 		maxwidth = 50, -- prevent the popup from showing more than provided characters (e.g 50 will not show more than 50 characters)
	-- 		ellipsis_char = '...', -- when popup menu exceed maxwidth, the truncated part would show ellipsis_char instead (must define maxwidth first)
	-- 		with_text = false
	-- 	})
	-- },
	snippet = {
		expand = function(args)
			luasnip.lsp_expand(args.body)
		end,
	},
	completion = {
		completeopt = "menu,menuone,noinsert",
	},
	-- window = {
	-- 	completion = cmp.config.window.bordered(),
	-- 	documentation = cmp.config.window.bordered(),
	-- },
	mapping = cmp.mapping.preset.insert({
		["<C-d>"] = cmp.mapping.scroll_docs(-4),
		["<C-f>"] = cmp.mapping.scroll_docs(4),
		["<C-e>"] = cmp.mapping.close(),
		["<C-Space>"] = cmp.mapping.complete(),
		["<CR>"] = cmp.mapping.confirm({
			behavior = cmp.ConfirmBehavior.Replace,
			select = true,
		}),
		["<Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_next_item()
			elseif luasnip.expand_or_jumpable() then
				luasnip.expand_or_jump()
			else
				fallback()
			end
		end, { "i", "s" }),
		["<S-Tab>"] = cmp.mapping(function(fallback)
			if cmp.visible() then
				cmp.select_prev_item()
			elseif luasnip.jumpable(-1) then
				luasnip.jump(-1)
			else
				fallback()
			end
		end, { "i", "s" }),
	}),
	sources = {
		{ name = "nvim_lsp" }, -- from language server
		{ name = "nvim_lsp_signature_help" }, -- display function signatures with current parameter emphasized
		{ name = "luasnip" },
		{ name = "path" }, -- file paths
		{ name = "nvim_lua" }, -- complete neovim's Lua runtime API such vim.lsp.*
		{ name = "buffer", keyword_length = 2 }, -- source current buffer
		{ name = "vsnip", keyword_length = 2 }, -- nvim-cmp source for vim-vsnip
	},
})

local signs = {
	Error = icons.error .. " ",
	Warn = icons.warn .. " ",
	Hint = icons.hint .. " ",
	Info = icons.info .. " ",
}

for type, icon in pairs(signs) do
	local hl = "DiagnosticSign" .. type
	vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = "" })
end

-- icon
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(vim.lsp.diagnostic.on_publish_diagnostics, {
	underline = true,
	-- This sets the spacing and the prefix, obviously.
	virtual_text = {
		spacing = 4,
		prefix = "",
	},
})
