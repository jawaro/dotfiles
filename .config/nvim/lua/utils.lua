local M = {}

function M.map(mode, lhs, rhs, opts)
	local options = { noremap = true }
	if opts then
		options = vim.tbl_extend("force", options, opts)
	end
	vim.api.nvim_set_keymap(mode, lhs, rhs, options)
end

function M.nmap(shortcut, command)
	M.map("n", shortcut, command, { silent = true })
end

function M.imap(shortcut, command)
	M.map("i", shortcut, command)
end

function M.vmap(shortcut, command)
	M.map("v", shortcut, command, { silent = true })
end

function M.cmap(shortcut, command)
	M.map("c", shortcut, command)
end

function M.tmap(shortcut, command)
	M.map("t", shortcut, command)
end

return M
