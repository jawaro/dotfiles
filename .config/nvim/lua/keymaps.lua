local map = require("utils").map
local nmap = require("utils").nmap
local vmap = require("utils").vmap
local imap = require("utils").imap

vim.g.mapleader = " "
vim.g.maplocalleader = " "

-- Center screen after vertical move
nmap("<C-d>", "<C-d>zz")
nmap("<C-u>", "<C-u>zz")

nmap("U", "<C-r>") -- U is useless, make redo instead
nmap("Q", "<nop>") -- Don't use Ex mode
nmap("Y", "y$") -- Make Y behave like D
nmap("J", "mzJ`z") -- Keep position when joining lines
nmap("<leader>h", ":set hlsearch!<cr>") -- Switch search highlight

-- Use tab to navigate curly braces instead of %
nmap("<tab>", "%")
vmap("<tab>", "%")

-- Switch between light and dark themes
nmap("<leader>cd", ":set background=dark<cr>")
nmap("<leader>cl", ":set background=light<cr>")

-- Use leader key instead of control to navigate to prev/next position
nmap("<leader>i", "<c-i>")
nmap("<leader>o", "<c-o>")

-- In insert mode, stop f*** your pinky hitting Esc all the time
imap("jk", "<esc>")

nmap("<leader>s", ":w<cr>") -- save file
nmap("<leader>w", ":Bdelete<cr>") -- close buffer
nmap("<leader><leader>", "<c-^>") -- switch to last buffer

-- reload config file
-- nmap("<leader>x", ":source ~/.config/nvim/init.lua<cr>")

-- easy window navigation with Tmux
map("n", "<C-h>", "<cmd> TmuxNavigateLeft<CR>")
map("n", "<C-j>", "<cmd> TmuxNavigateDown<CR>")
map("n", "<C-k>", "<cmd> TmuxNavigateUp<CR>")
map("n", "<C-l>", "<cmd> TmuxNavigateRight<CR>")

-- Split windows
map("n", "sh", ":split<Return><C-w>j")
map("n", "sv", ":vsplit<Return><C-w>l")

-- Keep cursor centered when searching and unfold
nmap("n", "nzzzv")
nmap("N", "nzNzzzv")

-- Move lines and indent
map("n", "<leader>j", ":m .+1<cr>==", { silent = true })
map("n", "<leader>k", ":m .-2<cr>==", { silent = true })
map("v", "J", ":m '>+1<cr>gv=gv", { silent = true })
map("v", "K", ":m '<-2<cr>gv=gv", { silent = true })

-- Disable arrow keys for navigation and use them for resizing
nmap("<Down>", ":resize -2<cr>")
nmap("<Left>", ":vertical resize +2<cr>")
nmap("<Right>", ":vertical resize -2<cr>")
nmap("<Up>", ":resize +2<cr>")

-- Sort lines, selected or over motion.
map("x", "gs", ":sort i<CR>", { silent = true })
map("v", "gs", ":set opfunc=SortLines<CR>g@", { silent = true })
vim.cmd([[
	fun! SortLines(type) abort
	'[,']sort i
	endfun
]])

-- Coverage
vim.keymap.set("n", "<leader>cc", "<cmd>Coverage<cr>", { desc = "Load and show [C]overage" })
vim.keymap.set("n", "<leader>ch", "<cmd>CoverageHide<cr>", { desc = "[H]ide coverage" })
vim.keymap.set("n", "<leader>cu", "<cmd>CoverageSummary<cr>", { desc = "Show [C]overage s[U]mmary" })

-- Diagnostic keymaps
vim.keymap.set("n", "<leader>e", vim.diagnostic.open_float, { desc = "Show diagnostics [E]rror messages" })
vim.keymap.set("n", "[d", vim.diagnostic.goto_prev)
vim.keymap.set("n", "]d", vim.diagnostic.goto_next)
vim.keymap.set("n", "<leader>q", vim.diagnostic.setloclist)

-- Spellcheck
nmap("<leader>cs", ":setlocal spell! spell?<cr>")
nmap("<leader>ce", ":setlocal spell spelllang=en_us<cr>")
nmap("<leader>cf", ":setlocal spell spelllang=fr<cr>")
nmap("<leader>cn", "]s")
nmap("<leader>cp", "[s")
