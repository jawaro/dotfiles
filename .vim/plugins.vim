call plug#begin(stdpath('data') . '/plugged')

" Utilities
Plug 'tpope/vim-sensible' " defaults everyone can agree on
Plug 'tpope/vim-commentary' " easy commenting
Plug 'tpope/vim-surround' " easily find and replace surrounding
Plug 'tpope/vim-projectionist' " alternate between file and test file
Plug 'tpope/vim-commentary' " easy commenting
Plug 'AndrewRadev/splitjoin.vim' " switch between single- and multi-line
Plug 'nvim-telescope/telescope.nvim' " fuzzy file finder
Plug 'nvim-telescope/telescope-fzf-native.nvim', { 'do': 'make' }
Plug 'kyazdani42/nvim-web-devicons' " for file icons
" Plug 'kyazdani42/nvim-tree.lua' " file explorer
Plug 'nvim-lua/plenary.nvim' " lua helpers used by other plugins
Plug 'moll/vim-bbye' " :Bdelete buffer w/o closing window
Plug 'nvim-neo-tree/neo-tree.nvim' " file explorer
Plug 'MunifTanjim/nui.nvim' " required by nvim-neo-tree

" Generic programming helpers
Plug 'windwp/nvim-autopairs'
Plug 'neoclide/coc.nvim', { 'branch': 'release' }
Plug 'fannheyward/telescope-coc.nvim'
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'} " treesitter
Plug 'nvim-treesitter/playground' " show treesitter nodes
Plug 'nvim-treesitter/nvim-treesitter-textobjects' " enable more advanced treesitter-aware text objects
Plug 'dense-analysis/ale' " linting and fixing
Plug 'tpope/vim-endwise' " automatically add end closings
Plug 'folke/trouble.nvim' " pretty list for diagnoses
Plug 'liuchengxu/vista.vim'

" Git
Plug 'airblade/vim-gitgutter'
Plug 'tpope/vim-fugitive'
Plug 'shumphrey/fugitive-gitlab.vim'

" Theme / Interface
" Plug 'onsails/lspkind-nvim' " show nerd fonts in lsp completion
Plug 'nvim-lualine/lualine.nvim'
Plug 'startup-nvim/startup.nvim' " cool startup window
Plug 'sainnhe/sonokai'
Plug 'edkolev/tmuxline.vim' " generate tmux theme
Plug 'projekt0n/github-nvim-theme'
Plug 'ellisonleao/gruvbox.nvim'
Plug 'shaunsingh/solarized.nvim'
Plug 'rakr/vim-one'

" Tmux
Plug 'benmills/vimux' " interact with tmux

" Test
Plug 'vim-test/vim-test'
" Plug 'kassio/neoterm'
" Plug 'rcarriga/vim-ultest', { 'do': ':UpdateRemotePlugins' }
Plug 'tpope/vim-dispatch' " asynchronous build and test dispatcher

" Markdown and writing
" Plug 'tpope/vim-markdown'
" Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app && yarn install'  }
Plug 'junegunn/goyo.vim' " distraction-free mode

" LaTeX
Plug 'lervag/vimtex'

" Javascript and Typescript
" Plug 'p00f/nvim-ts-rainbow' " rainbow highlighting to parens and brackets
Plug 'neoclide/vim-jsx-improve'

" HTML and CSS
" Plug 'tpope/vim-ragtag' " endings for html, xml
" Plug 'gregsexton/MatchTag', { 'for': 'html' } " match tags in html
" Plug 'othree/html5.vim', { 'for': 'html' } " html5 support
Plug 'norcalli/nvim-colorizer.lua' " add color to hex values
Plug 'mattn/emmet-vim' " emmet support
Plug 'neoclide/jsonc.vim'

" Elixir
Plug 'elixir-editors/vim-elixir'
Plug 'amiralies/coc-elixir', {'do': 'yarn install && yarn prepack'}
Plug 'spiegela/vimix' " elixir support for tmux

call plug#end()
