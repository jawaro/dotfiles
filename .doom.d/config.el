;;; $DOOMDIR/config.el -*- lexical-binding: t; -*-
;;;
(setq user-full-name "André Hoarau"
      user-mail-address "andre@hoarau.dev")

;; INTERFACE

;; Fringe
(setq-default fringe-mode 4)
(set-fringe-mode 4)

;; Frames
(after! ivy-posframe
  (setq ivy-posframe-display-functions-alist
        '(
          ;; (counsel-git-grep . ivy-display-function-fallback)
          ;; (counsel-grep . ivy-display-function-fallback)
          ;; (counsel-rg . ivy-display-function-fallback)
          ;; (swiper . ivy-display-function-fallback)
          (complete-symbol . ivy-posframe-display-at-point)
          (t               . ivy-posframe-display-at-frame-top-center))))
          ;; (t               . ivy-posframe-display-at-frame-bottom-center)
          

;; This determines the style of line numbers in effect. If set to `nil', line
;; numbers are disabled. For relative line numbers, set this to `relative'.
(setq display-line-numbers-type 'relative)
;; (setq highlight-indent-guides-method 'bitmap)
;; (set-face-background 'indent-guide-face "dimgray")


;; Better defaults
(setq-default x-stretch-cursor t
              window-combination-resize t)

(setq evil-want-fine-undo t
      truncate-string-ellipsis "…"
      scroll-margin 2
      blink-cursor-blinks -1
      display-fill-column-indicator-column 80
      indicate-empty-lines nil)

(blink-cursor-mode)
(global-subword-mode 0)

;; (add-hook 'prog-mode-hook #'display-fill-column-indicator-mode)
(add-hook 'elixir-mode-hook (lambda () display-fill-column-indicator-column 100))

;; Doom exposes five (optional) variables for controlling fonts in Doom:
;;
;; - `doom-font' -- the primary font to use
;; - `doom-variable-pitch-font' -- a non-monospace font (where applicable)
;; - `doom-big-font' -- used for `doom-big-font-mode'; use this for
;;   presentations or streaming.
;; - `doom-unicode-font' -- for unicode glyphs
;; - `doom-serif-font' -- for the `fixed-pitch-serif' face
;;
;; If you or Emacs can't find your font, use 'M-x describe-font' to look them
;; up, `M-x eval-region' to execute elisp code, and 'M-x doom/reload-font' to
;; refresh your font settings. If Emacs still can't find your font, it likely
;; wasn't installed correctly. Font issues are rarely Doom issues!

(setq doom-font (font-spec :family "JetBrains Mono" :size 19)
      doom-big-font (font-spec :family "JetBrains Mono" :size 29)
      doom-variable-pitch-font (font-spec :family "Cantarell" :size 16)
      doom-unicode-font (font-spec :family "JuliaMono")
      doom-serif-font (font-spec :family "Cantarell" :size 16)
      doom-themes-enable-italic t
      doom-themes-enable-bold t)

(setq doom-themes-enable-italic t
      doom-themes-enable-bold t)

;; Use more italics
(custom-set-faces!
  '(font-lock-keyword-face :slant italic)
  '(font-lock-comment-face :slant italic))

;; I want to breathe!
(defun set-bigger-spacing ()
  (setq-local default-text-properties '(line-spacing 0.25 line-height 1.25)))
(add-hook 'text-mode-hook 'set-bigger-spacing)
(add-hook 'prog-mode-hook 'set-bigger-spacing)
(add-hook 'magit-mode-hook 'set-bigger-spacing)

;; There are two ways to load a theme. Both assume the theme is installed and
;; available. You can either set `doom-theme' or manually load a theme with the
;; `load-theme' function. This is the default:
(setq doom-theme 'doom-tomorrow-night)

;; Set all-the-icons factor to 1.0 to prevent weird issues with the modeline.
(setq all-the-icons-scale-factor 1.0)

;; Modeline
(setq doom-modeline-major-mode-icon nil
      doom-modeline-buffer-file-name-style 'file-name
      doom-modeline-height 30
      doom-modeline-hud nil
      doom-modeline-lsp nil
      doom-modeline-time t
      doom-modeline-time-icon t)

(setq projectile-project-search-path '("~/src")
      projectile-globally-ignored-files (append '(".bloop" ".metals") projectile-globally-ignored-files))

(setq yas-snippet-dirs (append yas-snippet-dirs
                               '("~/.doom.d/snippets")))

;; If you use `org' and don't want your org files in the default location below,
;; change `org-directory'. It must be set before org loads!
(setq org-directory "~/org/")
(setq org-todo-keywords
      '((sequence "TODO" "DOING" "|" "DONE")))

;; Window-splitting
(setq evil-vsplit-window-right t
      evil-split-window-below t)

(map! :map evil-window-map
      "SPC" #'rotate-layout
      ;; Navigation
      "<left>"     #'evil-window-left
      "<down>"     #'evil-window-down
      "<up>"       #'evil-window-up
      "<right>"    #'evil-window-right
      ;; Swapping windows
      "C-<left>"       #'+evil/window-move-left
      "C-<down>"       #'+evil/window-move-down
      "C-<up>"         #'+evil/window-move-up
      "C-<right>"      #'+evil/window-move-right)

;; Format-on-save ignore list.
(setq +format-on-save-enabled-modes
      '(not scala-mode
        markdown-mode))

;;;; LSP
(when (or (modulep! :checkers syntax +flymake)
          (not (modulep! :checkers syntax)))
  (setq lsp-diagnostics-provider :flymake))

(after! lsp-mode
  (setq
   lsp-log-io nil
   lsp-auto-guess-root t
   lsp-progress-via-spinner t
   lsp-enable-file-watchers t
   lsp-idle-delay 0.47
   lsp-completion-enable-additional-text-edit t
   lsp-restart 'auto-restart

   lsp-signature-render-documentation t
   lsp-signature-auto-activate '(:on-trigger-char :on-server-request :after-completion)
   lsp-signature-doc-lines 15

   lsp-eldoc-enable-hover t
   lsp-headerline-breadcrumb-enable nil
   lsp-modeline-code-actions-segments '(count icon)

   lsp-enable-indentation nil
   lsp-enable-on-type-formatting nil
   lsp-enable-symbol-highlighting t
   lsp-enable-links nil
   lsp-lens-enable t
   lsp-headerline-breadcrumb-enable nil
   lsp-headerline-breadcrumb-enable-diagnostics nil

   lsp-elixir-fetch-deps nil
   lsp-elixir-suggest-specs nil
   lsp-elixir-signature-after-complete t
   lsp-elixir-enable-test-lenses t
   lsp-elixir-dialyzer-enabled t))

   

(when (modulep! :completion company)
  (setq +lsp-company-backends '(company-capf :with company-yasnippet)))

(after! lsp-ui
  (setq
   ;; Sideline
   lsp-ui-sideline-enable t
   lsp-ui-sideline-show-code-actions nil
   lsp-ui-sideline-show-symbol nil
   lsp-ui-sideline-show-hover nil
   lsp-ui-sideline-show-diagnostics t
   lsp-ui-sideline-ignore-duplicate t
   ;; Peek
   lsp-ui-peek-enable t
   ;; Doc
   lsp-ui-doc-enable t
   lsp-ui-doc-position 'at-point
   lsp-ui-doc-delay 0.51
   lsp-ui-doc-max-width 50
   lsp-ui-doc-max-height 30
   lsp-ui-doc-include-signature t
   lsp-ui-doc-header t))

;; LSP Origami
(setq lsp-enable-folding t)
(use-package! lsp-origami)
(add-hook! 'lsp-after-open-hook #'lsp-origami-try-enable)

;;;; Treemacs
(after! treemacs
  (set-popup-rule! "^ \\*Treemacs"
    :side 'left
    :slot 1
    :size 0.30
    :quit nil
    :ttl 0)
  (set-popup-rule! "^\\*LSP Symbols List\\*$"
    :side 'right
    :slot 2
    :size 0.30
    :quit nil
    :ttl 0)
  (setq doom-themes-treemacs-line-spacing 2
        doom-themes-treemacs-enable-variable-pitch t
        treemacs-width 40
        treemacs-is-never-other-window t
        treemacs-collapse-dirs 4)
  (setq-default treemacs-show-cursor t)
  (treemacs-follow-mode t)
  (treemacs-filewatch-mode t)
  (treemacs-fringe-indicator-mode 'always))

;;; Miscellaneous popup rules

;; (set-popup-rules!
;;   '(("^\\*info\\*"
;;      :slot 2 :side left :width 83 :quit nil)
;;     ("^\\*\\(?:Wo\\)?Man "
;;      :vslot -6 :size 0.45 :select t :quit nil :ttl 0)
;;     ("^\\*ielm\\*$"
;;      :vslot 2 :size 0.4 :quit nil :ttl nil)
;;     ("^\\*Ilist\\*$"
;;      :slot 2 :side left :size 0.3 :quit nil :ttl nil)
;;     ;; `help-mode', `helpful-mode'
;;     ("^\\*[Hh]elp"
;;      :slot 2 :vslot -8 :size 0.45 :select t)
;;     ("^\\*Checkdoc Status\\*$"
;;      :vslot -2 :select ignore :quit t :ttl 0)
;;     ("^\\*\\(?:[Cc]ompil\\(?:ation\\|e-Log\\)\\|Messages\\)"
;;      :slot -2 :size 0.45 :side right :autosave t :quit current :ttl nil
;;      :modeline t)
;;     ("^ \\*\\(?:undo-tree\\|vundo tree\\)\\*"
;;      :slot 2 :side left :size 20 :select t :quit t)
;;     ("^\\*\\(?:doom \\|Pp E\\)"  ; transient buffers (no interaction required)
;;      :vslot -3 :size +popup-shrink-to-fit :autosave t :select ignore :quit t :ttl 0)
;;     ("^\\*Backtrace" :vslot 99 :size 0.4 :quit nil)
;;     ("^\\*\\(?:Proced\\|timer-list\\|Process List\\|Abbrevs\\|Output\\|Occur\\|unsent mail\\)\\*" :ignore t)
;;     ("^\\*Flycheck errors\\*$"
;;      :vslot -2 :select t :quit t :ttl 0)))


;;; Bindings overrides

(map! (:map +tree-sitter-outer-text-objects-map
            "if" (evil-textobj-tree-sitter-get-textobj "function.inner")
            "af" (evil-textobj-tree-sitter-get-textobj "function.outer"))
      (:map +tree-sitter-inner-text-objects-map
            "if" (evil-textobj-tree-sitter-get-textobj "function.inner")
            "af" (evil-textobj-tree-sitter-get-textobj "function.outer"))
      (:map evil-treemacs-state-map
            "M-n" #'multi-next-line
            "M-p" #'multi-previous-line)
      :n "C-h" #'evil-window-left
      :n "C-j" #'evil-window-bottom
      :n "C-k" #'evil-window-up
      :n "C-l" #'evil-window-right

      :nvo "C-t" #'+vterm/toggle
      :nvo "C-p" #'projectile-find-file
      :n "C-f" #'evil-avy-goto-char-2

;;;; Avy
      ;; :nvo "f" #'evilem-motion-find-char
      ;; :nvo "F" #'evilem-motion-find-char-backward
      ;; :nvo "t" #'evilem-motion-find-char-to
      ;; :nvo "T" #'evilem-motion-find-char-to-backward
      ;; (:leader
      ;;  :desc "Go to line above" "k" #'evilem-motion-previous-line-first-non-blank
      ;;  :desc "Go to line below" "j" #'evilem-motion-next-line-first-non-blank
      ;;  :desc "Go to line" "l" #'avy-goto-line)

      (:leader
       :desc "Switch to last buffer" "SPC"    #'evil-switch-to-windows-last-buffer
       ;; Quit
       (:prefix "q"
        :desc "Restart Emacs"                "r" #'doom/restart
        :desc "Restart & restore Emacs"      "R" #'doom/restart-and-restore
        :desc "Save buffers and kill server" "Q" #'save-buffers-kill-emacs)
       ;; Magit
       (:prefix "v"
        :desc "Magit file dispatch"          "d" #'magit-file-dispatch
        :desc "Create or checkout branch"    "b" #'magit-branch-or-checkout))

;;;; Hydra
      (:leader
       (:when (modulep! :ui hydra)
         (:prefix "w"
          :desc "Interactive menu"    "w" #'+hydra/window-nav/body)
         (:prefix ("z" . "zoom")
          :desc "Text"                "t" #'+hydra/text-zoom/body)))

;;;; Flycheck
      (:after flycheck
              (:map flycheck-mode-map
                    "M-n" #'flycheck-next-error
                    "M-p" #'flycheck-previous-error))
              ;; (custom-set-faces!
              ;;   ;; '(flycheck-error :underline (:color "red2" :style line))
              ;;   ;; '(flycheck-error :underline nil)
              ;;   '(flycheck-error :underline nil :box "red2")
              ;;   )
              

      ;;;; Languages
      ;; Rust
      (:after rustic
              (:map rustic-mode-map
               :localleader
               (:prefix ("r" . "Rustic")
                :desc "Clippy pretty"     "C" #'rustic-cargo-clippy
                :desc "Popup"             "r" #'rustic-popup
                :desc "Format everything" "f" #'rustic-cargo-fmt
                :desc "Cargo-outdated"    "u" #'rustic-cargo-outdated)))

      ;; Python
      (:after python
              (:map python-mode-map
               :localleader
               :desc "Blacken buffer" "cb" #'blacken-buffer)))

(after! evil
  (setq evil-ex-substitute-global t     ; I like my s/../.. to by global by default
        evil-move-cursor-back nil       ; Don't move the block cursor when toggling insert mode
        evil-kill-on-visual-paste nil)) ; Don't put overwritten text in the kill ring

(define-derived-mode heex-mode web-mode "HEEx" "Major mode for editing HEEx files")
(add-to-list 'auto-mode-alist '("\\.heex?\\'" . heex-mode))
(after! tree-sitter
  (add-to-list 'tree-sitter-load-path (file-name-as-directory "~/.local/share/tree-sitter/"))
  (tree-sitter-load 'heex)
  (add-to-list 'tree-sitter-major-mode-language-alist '(heex-mode . heex)))


(setq alchemist-mix-env "dev")
;; Whenever you reconfigure a package, make sure to wrap your config in an
;; `after!' block, otherwise Doom's defaults may override your settings. E.g.
;;
;;   (after! PACKAGE
;;     (setq x y))
;;
;; The exceptions to this rule:
;;
;;   - Setting file/directory variables (like `org-directory')
;;   - Setting variables which explicitly tell you to set them before their
;;     package is loaded (see 'C-h v VARIABLE' to look up their documentation).
;;   - Setting doom variables (which start with 'doom-' or '+').
;;
;; Here are some additional functions/macros that will help you configure Doom.
;;
;; - `load!' for loading external *.el files relative to this one
;; - `use-package!' for configuring packages
;; - `after!' for running code after a package has loaded
;; - `add-load-path!' for adding directories to the `load-path', relative to
;;   this file. Emacs searches the `load-path' when you load packages with
;;   `require' or `use-package'.
;; - `map!' for binding new keys
;;
;; To get information about any of these functions/macros, move the cursor over
;; the highlighted symbol at press 'K' (non-evil users must press 'C-c c k').
;; This will open documentation for it, including demos of how they are used.
;; Alternatively, use `C-h o' to look up a symbol (functions, variables, faces,
;; etc).
;;
;; You can also try 'gd' (or 'C-c c d') to jump to their definition and see how
;; they are implemented.

                                        ; (use-package! font-lock
                                        ;   :defer t
                                        ;   :custom-face
                                        ;   (font-lock-comment-face ((t (:inherit font-lock-comment-face :italic t))))
                                        ;   (font-lock-doc-face ((t (:inherit font-lock-doc-face :italic t))))
                                        ;   (font-lock-string-face ((t (:inherit font-lock-string-face :italic t)))))

(use-package!
    polymode
  :ensure t
  :mode ("\.ex$'" . poly-elixir-web-mode)
  :init (setq web-mode-engines-alist '(("elixir" . "\\.ex\\'")))
  :config
  (define-hostmode poly-elixir-hostmode :mode 'elixir-mode)
  (define-innermode poly-surface-expr-elixir-innermode
    :mode 'heex-mode
    :head-matcher (rx line-start (* space) "~H" (= 3 (char "\"'")) line-end)
    :tail-matcher (rx line-start (* space) (= 3 (char "\"'")) line-end)
    :head-mode 'host
    :tail-mode 'host
    :allow-nested nil
    :keep-in-mode 'host
    :fallback-mode 'host)
  (define-polymode poly-elixir-web-mode
    :hostmode 'poly-elixir-hostmode
    :innermodes '(poly-surface-expr-elixir-innermode)))
