#!/bin/sh

set -x

SESSION="dev"
SESSION_EXISTS=$(tmux list-sessions | grep $SESSION)

if [ "$SESSION_EXISTS" = "" ]; then
    tmux new-session -d -s $SESSION -n main
    tmux send-keys -t $SESSION:main "nvim" C-m
#    tmux split-window -v -p 10 -t $SESSION:main
fi

tmux attach-session -t $SESSION:1.1
