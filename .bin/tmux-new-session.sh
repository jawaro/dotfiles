#!/usr/bin/env bash

# don't nest tmux sessions
# if [ -n "$TMUX" ]; then
#     exit 0;
# fi

PS3="Choose your session: "
IFS=$'\n' && options=("New Session" $(tmux list-sessions -F "#S" 2>/dev/null))
echo "Available sessions"
echo " "
select opt in "${options[@]}"
do
    case $opt in
        "New Session")
            read -rp "Enter new session name: " SESSION_NAME
            tmux new -s "$SESSION_NAME"
            break
            ;;

        *)
            tmux attach-session -t "$opt"
            break
            ;;
    esac
done
