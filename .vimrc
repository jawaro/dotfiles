" ~/.vimrc
" André Hoarau <ah@ouvaton.org>

set nocompatible " modern vim

" Editing
set encoding=utf8
set clipboard=unnamed " use system's clipboard
set backspace=indent,eol,start
set complete-=i
set completeopt=menu,preview
set modelines=0
set nomodeline

set hidden " allow unsaved background buffers and remember marks/undo for them
set linebreak " don't display wrap
set textwidth=80
set wrapmargin=8
set nostartofline

set laststatus=3
set noshowmode
set cursorline

set nojoinspaces " use one space, not two, after punctuation

" Errors
set noerrorbells
set visualbell
set confirm

set shortmess+=c " Don't pass messages to |ins-completion-menu|.

" Folds
set foldenable
set foldmethod=expr
set foldexpr=nvim_treesitter#foldexpr()
set foldlevelstart=10 " open most folds by default
set foldnestmax=10
set fillchars=fold:\ ,

" Search and replace
set hlsearch " highlight search results
set smartcase " ignore case when searching lowercase
set incsearch " incremental search, search as you type
set gdefault " use 'g' by default for substitutions, type 'g' for only one
if has('nvim')
    set inccommand=nosplit
end

" tab and indent
set autoindent
set expandtab " insert spaces when tab is pressed
set nowrap " don't wrap text
set shiftwidth=4
set smartindent
set smarttab
set shiftround
set softtabstop=4
set tabstop=4 " one tab == four spaces

" Interface
set showbreak=↪
set showmatch " show matching bracket
set cmdheight=1
set diffopt=vertical " diffs are shown side-by-side, not above/below
set display+=lastline
set lazyredraw
set number " show line numbers
set numberwidth=5
set previewheight=30
set ruler " always show current position
set scrolloff=4 " alway show some lines above/below cursor
set showcmd
set sidescrolloff=5
set termguicolors " enable true colors
set title
set signcolumn=yes
set updatetime=50

" Resize split when window is resized
autocmd VimResized * :wincmd =

" case-insensitive tab-completion
set wildmenu
set wildmode=list:longest,list:full
set wildignorecase
set showfulltag

if has('nvim')
    set winblend=5 " enable pseudo-transparency for floating windows
    set pumblend=5 " enable pseudo-transparency for popups
endif

if exists('+colorcolumn')
    set colorcolumn=80
    highlight ColorColumn ctermbg=DarkGray
else
    au BufWinEnter * let w:m2=matchadd('ErrorMsg', '\%>80v.\+', -1)
endif

" Show relative numbers in normal mode and absolute number in insertion mode
set relativenumber
augroup toggle_relative_number
    autocmd InsertEnter * :setlocal norelativenumber
    autocmd InsertLeave * :setlocal relativenumber
augroup End

" enable mouse in normal and visual modes
if has('mouse')
    set mouse=nv
end

syntax enable
filetype plugin indent on

" Swaps and backups
set noswapfile " don't create swap file, 'cause I save often and use tmux
set nobackup " don't make backup
set autowrite " automatically :write before running some commands
set autoread " autoread when file has been changed outside of Vim
au FocusGained,BufEnter * :checktime

" Undo
set undolevels=1000
" Use persistent undo history.
if !isdirectory("/tmp/.vim-undo-dir")
    call mkdir("/tmp/.vim-undo-dir", "", 0700)
endif
set undodir=/tmp/.vim-undo-dir
set undofile

" timeout
set timeoutlen=400
if !has('nvim') && &ttimeoutlen == -1
  set ttimeout
  set ttimeoutlen=100
endif

" Display extra whitespace
" set list listchars=tab:\▏\ ,trail:·,precedes:←,extends:→,eol:↲,nbsp:␣
set nolist

" Make vertical separator pretty
highlight VertSplit cterm=NONE
set fillchars+=vert:\▏

set history=1000
set tabpagemax=50
set viminfo^=!
set sessionoptions-=options
set viewoptions-=options

if v:version > 703 || v:version == 703 && has("patch541")
  set formatoptions+=j " Delete comment character when joining commented lines
endif

" open new split panes to right right and bottom
set splitbelow
set splitright

" ============================================================ General mappings

let mapleader = " "

map <F7> :tabp<CR>
map <F8> :tabn<CR>

" U is useless (except for Vi compatibility), make redo instead
nnoremap U <C-r>

" Don't use Ex mode
nnoremap Q <nop>

" Show search
nmap <leader>h :set hlsearch! hlsearch?<cr>

" use tab to navigate curly braces instead of %
nnoremap <tab> %
vnoremap <tab> %

nnoremap <leader>cd :set background=dark<cr>
nnoremap <leader>cl :set background=light<cr>

nnoremap <leader>o <c-o>
nnoremap <leader>i <c-i>

" use ;; to escape
noremap ;; <esc>

" In insert mode, stop f*** your pinky hitting Esc all the time
inoremap jk <esc>
" Also in visual mode!
vnoremap ii <esc>
" save file
nnoremap <leader>s :w<cr>
" close buffer
nnoremap <leader>w :Bdelete<cr>
" switch to last buffer
nnoremap <leader><leader> <c-^>
" move to the next buffer
" nnoremap <leader>l :bnext<cr>
" move to the previous buffer
" nnoremap <leader>h :bprevious<cr>
" reload config file
nnoremap <leader>x :source ~/.config/nvim/init.vim<cr>

" easy window navigation
map <C-h> <C-w>h
map <C-j> <C-w>j
map <C-k> <C-w>k
map <C-l> <C-w>l

nnoremap Y y$

" keep cursor centered when searching and unfold
nnoremap n nzzzv
nnoremap N Nzzzv

" keep cursor position when joining lines
nnoremap J mzJ`z

" move lines and indent
nnoremap <silent> <leader>k :m .-2<cr>==
vnoremap <silent> J :m '>+1<cr>gv=gv
nnoremap <silent> <leader>j :m .+1<cr>==
vnoremap <silent> K :m '<-2<cr>gv=gv

" disable arrow keys and use them for resizing
nnoremap <Down> :resize -2<cr>
nnoremap <Left> :vertical resize +2<cr>
nnoremap <Right> :vertical resize -2<cr>
nnoremap <Up> :resize +2<cr>

" Sort lines, selected or over motion.
xnoremap <silent> gs :sort i<CR>
nnoremap <silent> gs :set opfunc=SortLines<CR>g@
fun! SortLines(type) abort
    '[,']sort i
endfun

" projectionist: switch to alternate file, e.g. to test file
" this is SO great!
nnoremap <leader>; :A<cr>
nnoremap <leader>p :A<cr>
